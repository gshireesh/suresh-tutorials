build:
	docker-compose build

serve:
	docker-compose up

up:
	docker-compose up -d

down:
	docker-compose down

restart:
	docker-compose restart

start:
	docker-compose start

stop:
	docker-compose stop

shell-web:
	docker exec -ti suresh_web bash
#
# rebuild:
#     make build && make up
